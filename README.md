![stability-wip](https://bitbucket.org/repo/ekyaeEE/images/477405737-stability_work_in_progress.png)
![stability-wip](https://bitbucket.org/repo/ekyaeEE/images/3847436881-internal_use_stable.png)

# Rationale #
* Some tutorials to share with all the foreign people that visit our institution. A concise, minimalistic approach with a focus in simplicity and easy understanding in the steps to follow to connect with our proxy server.

### What is this repository for? ###

* A myriad of tutorials that explain the steps to follow to connect to our wifi connection through our proxy server.
* Version 1.2

### How do I get set up? ###
* Summary of set up
	- a device with wifi capabilities (desktop, mobile phones, tablets, notebooks)
    - a wifi connection to read this repository, _hence_, to read all the data collected and shared.
	- a markdown viewer (_optional_) (see below) or pdf reader (in case you choose the pdf version)
		
* Dependencies
     - There is no dependencies
	 
* Deployment instructions
     - Tutorials can be downloaded in the [Download](https://bitbucket.org/imhicihu/proxy-settings-tutorials/downloads/) section. 
	 - All of them are in `markdown` format.
	 - So, they can be seen once you install one of these tools
	 	- [Markdown Live Preview](http://markdownlivepreview.com/)
		- [Markdown Viewer](https://github.com/simov/markdown-viewer): compatible with Mozilla Firefox & Google Chrome
		![68747470733a2f2f692e696d6775722e636f6d2f724e53394144572e706e67.png](https://bitbucket.org/repo/4pKrXRd/images/483318999-68747470733a2f2f692e696d6775722e636f6d2f724e53394144572e706e67.png)
		- [Stackedit](https://stackedit.io/app#)
		- [MacDown](https://macdown.uranusjr.com/) markdown editor and viewer for the mac
		![macdown-demo.png](https://bitbucket.org/repo/4pKrXRd/images/757285596-macdown-demo.png)
		
### Related repositories

* [Conferences](https://bitbucket.org/imhicihu/conferences/src/master/)
* [Streaming](https://bitbucket.org/imhicihu/streaming/src/master/)
* [Setting up Github under proxy](https://bitbucket.org/imhicihu/setting-up-github-under-proxy/src/master/)
* [NPM under proxy (settings)](https://bitbucket.org/imhicihu/npm-under-proxy-settings/src/master/)

### Changelog ###
* Please check the [Commits](https://bitbucket.org/imhicihu/proxy-settings-tutorials/commits/) section for the current status

### Contribution guidelines ###
* Writing tests or data review
     - Contact us through our [board](https://bitbucket.org/imhicihu/proxy-settings-tutorials/addon/trello/trello-board) on `Proposals - Propuestas` section. (You need a [Trello](https://trello.com/) account)

### Who do I talk to? ###
* Repo owner or admin:
     - Contact `imhicihu` at `gmail` dot `com`

### Copyright ###
![88x31.png](https://bitbucket.org/repo/4pKrXRd/images/3902704043-88x31.png)
This work is licensed under a [Creative Commons Attribution-ShareAlike 2.0 Generic License](http://creativecommons.org/licenses/by-sa/2.0/).

### Code of Conduct

* Please, check our [Code of Conduct](https://bitbucket.org/imhicihu/proxy-settings-tutorials/src/master/code_of_conduct.md)


### Legal ###
* All trademarks are the property of their respective owners. 