Utilización del WIFI fuera del ámbito geográfico del IMHICIHU

Si vamos a utilizar Internet Explorer para navegar por internet, seguir estos pasos

1- lanzar Internet Explorer 
2- en el menú que ofrece este programa, ir a Tools
3- después a Internet Options
4- Aparecerá un cuadro de dialogo, llamado precisamente Internet Options
5- hacer click en el tabulado llamado Connections
6- En la sección Connections, dirigirse al final, donde dice Local Area Network (LAN) SETTINGS.
7- Click en LAN settings
8- Aparecerá un cuadro de diálogo llamado Local Area Network (LAN) settings.
9- Dirigirse a la sección Proxy Server

IMPORTANTE

10- Si la computadora se la va a utilizar FUERA del IMHICIHU, desclickear donde dice
Use a proxy server for your LAN (These settings will not apply to dial-up or VPN connections).
Address: 192.168.8.17     Port: 3128

Veremos que si se desclickea esta opción, dichos números se ponen en gris. 
Esta acción habilita a que esta computadora pueda comunicarse con cualquier red WIFI que esté a su alcance.

11- Click en OK.
12- cLICK en Apply.
13- Click en OK.
14- Su computadora puede navegar en cualquier red de WIFI que esté a su alcance.


Si vamos a utilizar Mozilla Firefox para navegar por internet, seguir estos pasos

1- lanzar Mozilla Firefox 
2- en el menú que ofrece este programa, ir a Tools
3- después a Options
4- Aparecerá un cuadro de dialogo, llamado precisamente Options
5- hacer click en el tabulado llamado Advanced
6- Aparecerá cuatro tabulados: General, Network, Update, Encryption
7- click en Network
8- En la sección Connection, click en Settings

IMPORTANTE

10- Si la computadora se la va a utilizar FUERA del IMHICIHU, DESCLICKEAR donde dice
Manual proxy configuration
HTTP Proxy: 192.168.8.17     Port: 3128  (por default está con una tilde en donde dice Use this proxy server for all protocols...
11- Y ahora hacemos click en la primera opción, esa que dice Direct connection to the Internet.
12- Después damos OK

13- Damos click nuevamente en OK.
14- Nuestra computadora ahora está habilitada para navegar en cualquier red de WIFI que esté a su alcance. 








En ambos casos y con sendos navegadores (Explorer y Firefox), el secreto es deshabilitar el proxy que la RED CONSORCIO (red de Saavedra 15) nos obliga para acceder a sus servicios.
y que claramente se especificó cuando señalé con esta palabra IMPORTANTE el momento preciso que es cuando lo deshabilitamos.



Para utilizar la Red Consorcio, precisamente hay que habilitar el proxy. Así como señalé cuando se debe desclickear, en este caso, el paso a seguir
es afirmar que deseamos usar ese procedimiento).

Ejemplo:

en Mozilla Firefox seguir este procedimiento

1- lanzar Mozilla Firefox 
2- en el menú que ofrece este programa, ir a Tools
3- después a Options
4- Aparecerá un cuadro de dialogo, llamado precisamente Options
5- hacer click en el tabulado llamado Advanced
6- Aparecerá cuatro tabulados: General, Network, Update, Encryption
7- click en Network
8- En la sección Connection, click en Settings

IMPORTANTE

10- Si la computadora se la va a utilizar DENTRO del IMHICIHU, CLICKEAR donde dice
Manual proxy configuration
HTTP Proxy: 192.168.8.17     Port: 3128  
11- Después damos OK
12- Damos click nuevamente en OK.
14- Nuestra computadora ahora está habilitada para navegar