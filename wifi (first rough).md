* Habilitar acceso wifi en su notebook
* Solicite un `nombre de usuario` (`username`) y `contraseña` (`password`) al personal del instituto.
* Elegir la red inalámbrica `RED_IMHICIHU` o `IMHICIHU 2018`
* Click en conectar
* Dentro de unos segundos, una vez que nos asigna una dirección de IP aleatoria dentro de la red, nos alertará que nos hemos conectado.
* Lanzar nuestro navegador (recomendable: [Google Chrome](https://www.google.com/chrome) o [Mozilla Firefox](https://www.mozilla.org/es-AR/firefox/new/)). 
* Si es Mozilla Firefox, deberá asignar esta dirección proxy para navegar 
    - Ir a `Herramientas`
	- `Opciones`
	- `Avanzado`
	- `Red`
	- Seteado
	- Configuración manual de proxy
	- `Proxy HTTP`
	- introducir esta dirección `192.168.4.1` y el puerto es `3128`. Asimismo tildamos afirmativamente en la opción `Usar este servidor proxy para todos los protocolos`.
	- `OK`
	- `OK`
	- Ya estamos habilitados para navegar. Introduzca la dirección de internet deseada y en segundos le pedirá un nombre de usuario y su correspondiente contraseña. Ya estará navegando.
* si el navegador es Google Chrome, seguir estos pasos:
	- Clic sobre el ícono de la derecha que asemeja a una llave inglesa
	- Clic en `Opciones`
	- Clic en `Avanzadas`
	- Clic sobre el botón `Cambiar la configuración del proxy`
	- Nos llevará a un cuadro de diálogo. 
	- Clic sobre `Conexiones`
	- Clic sobre `Configuración LAN`
	- Clic sobre la opción: Usar un servidor proxy para su LAN
	- Introducir esta dirección: `192.168.4.1` y su puerto es `3128`.
	- Clic en `OK`
	- Clic en `OK`
	- Clic en `Cerrar`
	- Ya estamos habilitados para navegar. Introduzca la dirección de internet deseada y en segundos le pedirá un nombre de usuario y su correspondiente contraseña. Ya estará navegando.



_Nota:_ para reestablecer las conexiones de vuestras respectivas conexiones de banda ancha, lo único que hay que hacer es desactivar la dirección proxy anteriormente señalada. Así, si fuera el caso de Google Chrome, seguir los pasos señalados, pero llegado el momento deberemos destildar la opción `Servidor proxy`. Verán que los números `192.168.4.1.` se pondrán en gris. Esto nos habilita a utilizar cualquier red wifi, sea en nuestro hogar, bar, aeropuerto, etc.
Si fuera el caso de Mozilla Firefox, seguir los pasos señalados, pero llegado el momento deberemos destildar la opción `Configuración manual de proxy` y elegir `Ningún proxy` (`no proxy`). Click en `Aceptar`, aceptar nuevamente. Ya estamos habilitados a utilizar cualquier red wifi.
