## Technical requirements ##

* Hardware
     - Macbook 13"
     - Macbook 15"
* Software
     - Wamp
     - Lamp
     - Mamp
     - [Carbon](https://carbon.now.sh/): (Automatization on code screen sharing)
	 - [Texts](http://www.texts.io/): for mac and pc environments
	 - [Writemonkey](http://writemonkey.com/wm3/): for mac and pc environments
     
## Legal ##

* All trademarks are the property of their respective owners.