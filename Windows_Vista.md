### Operating systems: ###
* Microsoft Windows Vista (32 & 64 bits)

### Procedures ###
* Enable your built-in wifi card
* Click `Start`
* Click `Control Panel`
* Click on `Internet Options`
* Click on tab `Connections`
* Click on `Lan settings`
* Go to `Proxy server` section
* on `Address:` add `192.168.100.20`
* on `Port:` add `3128`
* Click on `Advanced`
* Click on `Use the same proxy server for all protocols`
* Click `Ok`
* Click `Ok`
* Click `Ok`
* Now select `RED_IMHICIHU` or `IMHICIHU 2018`