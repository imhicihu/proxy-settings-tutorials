### Operating systems tested:
   * Snow Leopard (10.6)
   * Lion (10.7)
   * Mountain Lion (10.8)
   * Mavericks (10.9)
   * Yosemite (10.10)
   * El Capitan (10.11)
   * High Sierra (10.13)


### Procedures
   * Turn-on your Wi-Fi
   * Click on Apple Menu (top left of your screen)
   * Click on `System Preferences`
   * Click on `Network`
   * on `Network Name:` select `RED_IMHICIHU` or `IMHICIHU 2018`
   * Click on `Advanced`
   * Click on `Proxies`
   * on `Select a protocol to configure` enable this protocols: 
      - `Web proxy (HTTP)` and on `Web Proxy server` add `192.168.4.1` : `3128`. 
      - `Secure Web Proxy (HTTPS)` and on `Secure Web Proxy server` add `192.168.4.1` : `3128`. 
      - `FTP Proxy` and on `FTP Proxy server` add `192.168.4.1` : `3128`.
      - `SOCKS Proxy` and on `SOCKS Proxy server` add `192.168.4.1` : `3128`.
      - `Streaming Proxy (RTSP)` and on `Streaming Proxy server` add `192.168.4.1` : `3128`.
   * Click on `OK`
   * Click on `Apply`
   * Click on the top left red button
   * Your system is ready to surf the web inside our building.